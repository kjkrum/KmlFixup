﻿using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace KmlFixup
{
	public class Program
	{
		public static void Main()
		{
			// original KML from https://gbp-blm-egis.hub.arcgis.com/datasets/blm-az-surface-management-agency-polygon/explore
			var inputStream = typeof(Program).Assembly.GetManifestResourceStream("KmlFixup.Input.kml")!;
			var input = XElement.Load(inputStream);

			var sb = new StringBuilder(16_000_000);
			sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?><kml xmlns=\"http://www.opengis.net/kml/2.2\"><Document>");
			sb.Append(Style("USFS", Color.ForestGreen));
			sb.Append(Style("BLM", Color.SaddleBrown));
			sb.Append(Style("USFWS", Color.Goldenrod));
			sb.Append(Style("State", Color.SteelBlue));

			var regex = new Regex("^(USFS|BLM|USFWS|State)$", RegexOptions.Compiled);

			foreach (var placemark in input.Placemarks())
			{
				var style = placemark.SimpleData("CATEGORY")!;
				if(regex.IsMatch(style))
				{
					var geometry = placemark.Geometry();
					sb.Append(Placemark(style, geometry));
				}
			}

			sb.Append("</Document></kml>");
			var output = XElement.Parse(sb.ToString());
			using var writer = new StreamWriter("C:\\Users\\Kevin\\Desktop\\AZ_Hunt.kml");
			writer.Write(output.ToString());
		}

		private static string Style(string name, Color color) => $"<Style id =\"{name}\"><LineStyle><color>{color.ToKml()}</color></LineStyle></Style>";

		private static string Placemark(string style, string geometry) => $"<Placemark><styleUrl>#{style}</styleUrl>{geometry}</Placemark>";
	}
}