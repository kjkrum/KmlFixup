﻿using System.Drawing;
using System.Xml.Linq;

namespace KmlFixup
{
	public static class Extensions
	{
		public static IEnumerable<XElement> Placemarks(this XElement doc) => doc.Descendants().Where(x => x.Name.LocalName == "Placemark");

		public static string? SimpleData(this XElement node, string name) => node.Descendants().SingleOrDefault(x => x.Name.LocalName == "SimpleData" && x.Attribute("name")?.Value == name)?.Value;

		public static string Geometry(this XElement node) => node.Descendants().Single(x => x.Name.LocalName == "MultiGeometry").ToString();

		public static string ToKml(this Color color) => $"{color.A:X2}{color.B:X2}{color.G:X2}{color.R:X2}";
	}
}
